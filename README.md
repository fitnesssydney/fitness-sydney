# Introduction #

* [Fitness Sydney](https://bitbucket.org/fitnesssydney/fitness-sydney/) aims to record the data of exercise from users and motivate people participate in sport. 
* The SpringNVC method, Fitbit API and Google map API are used in this project. 
* This project involved a complete sign-on and upload system that allow users upload their individual data. In addition, analysis and feedback system related to fitness are equipped and the individual subproject will involve tracking, recording, sharing and comparing in web application. In addition, users could make their personal fitness plan through different API.


# Group Members #

* 440062987 - Bo Zhao
* 450429811 - Hongyang Nong


# Project Wiki #
* See our project wiki at [here](https://bitbucket.org/fitnesssydney/fitness-sydney/wiki/Home).


# Code #
* The code is available at [here](https://bitbucket.org/fitnesssydney/fitness-sydney/src).

## Video ##

https://youtu.be/p3U3btGLaeY