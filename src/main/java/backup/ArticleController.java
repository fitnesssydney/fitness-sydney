package backup;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.service.ArticleManager;


@Controller
@RequestMapping(value="/article/**")
public class ArticleController {
	
	@Resource(name="articleManager")
	private ArticleManager articleManager;

	@RequestMapping(value="/add")
	public String addArticle(Model uiModel) {
		
		return "adda";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addArticle(HttpServletRequest httpServletRequest) {
		
		Article product = new Article();
		product.settitle(httpServletRequest.getParameter("title"));
		product.setcontent(httpServletRequest.getParameter("content"));
		this.articleManager.addArticle(product);
		
		return "redirect:/myhome.htm";
	}
	
	@RequestMapping(value="/edit/{number}", method=RequestMethod.GET)
	public String editArticle(@PathVariable("number") Long number, Model uiModel) {
		
		Article product = this.articleManager.getArticleById(number);
		uiModel.addAttribute("article", product);
		
		return "edita";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editArticle(@Valid Article article) {
		
		this.articleManager.updateArticle(article);
		System.out.println(article.getnumber());
		
		return "redirect:/myhome.htm";
	}
	
	@RequestMapping(value="/delete/{number}", method=RequestMethod.GET)
	public String deleteArticle(@PathVariable("number") Long number) {
		
		this.articleManager.deleteArticle(number);
		
		return "redirect:/myhome.htm";
	}
}
