package backup;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;


import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.Plan;
import au.usyd.elec5619.service.UserManager;
import au.usyd.elec5619.service.PlanManager;

@Controller
@RequestMapping(value="/mydata/**")
public class MydataController {
	 @Resource(name="userManager")
		private UserManager userManager;

    @RequestMapping(value="/mydata", method=RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		HttpSession session = request.getSession();
		 long nu=((User) session.getAttribute("user")).getnumber();
			//user = (User) list.get(0);
			//Article article = (Article) deptInfoDao.findById(user.getDept());
//			httpSession.setAttribute("user", user);  
			 Map<String, Object> map = new HashMap<String, Object>();		
			//map.put("user", user);
			
			map.put("user",this.userManager.getUserById(nu) ); 
//			uupload=user.getnumber();
			
			//map.put("articles", this.articleManager.getAllArticles()); 
			return new ModelAndView("mydata", "model", map);       
			   
    }
    
}

