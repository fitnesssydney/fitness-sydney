package au.usyd.elec5619.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;


import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.Po;
import au.usyd.elec5619.service.UserManager;
import au.usyd.elec5619.service.ArticleManager;
import au.usyd.elec5619.service.PoManager;

@Controller
@RequestMapping(value="/record/**")
public class RecordController {
	
   @Resource(name="poManager")
	private PoManager poManager;	
  
	@RequestMapping(value="/add")
	public String addPo(Model uiModel) {
		
		return "record";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addPo(HttpServletRequest httpServletRequest) {
		
		Po po = new Po();
	    po.setx(httpServletRequest.getParameter("x"));
		po.sety(httpServletRequest.getParameter("y"));
//		po.setx(httpServletRequest.getParameterValues("x"));
//		po.sety(httpServletRequest.getParameterValues("y"));
		
		this.poManager.addPo(po);
		return "redirect:/record/add.htm";
		//return "redirect:/myhome.htm";
	}
	
//	@RequestMapping(value="/mydata")
//	public String go(Model uiModel) {
//		
//		return "mydata";
//	}
// 
    
//    @RequestMapping(value="/distance", method=RequestMethod.GET)
//    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
////        String now = (new java.util.Date()).toString();
////        logger.info("returning hello view with " + now);
//
//        Map<String, Object> myModel = new HashMap<String, Object>();
////        HttpSession session=request.getSession();
////        long nu=((User) session.getAttribute("user")).getnumber();
//        myModel.put("po", this.poManager.getAllPos());
//
//
//        return new ModelAndView("distance", "model", myModel);
//    }

	
	
}
