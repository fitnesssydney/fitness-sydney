package au.usyd.elec5619.web;

//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.ArticleManager;
@Controller
@RequestMapping(value="/article/**")
public class InventoryControllerA {

    protected final Log logger = LogFactory.getLog(getClass());
    @Resource(name="articleManager")
    private ArticleManager articleManager;
    @RequestMapping(value="/myhome", method=RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        String now = (new java.util.Date()).toString();
//        logger.info("returning hello view with " + now);

        Map<String, Object> myModel = new HashMap<String, Object>();
        HttpSession session=request.getSession();
        long nu=((User) session.getAttribute("user")).getnumber();
        myModel.put("articles", this.articleManager.getArticlesByuupload(nu));

        return new ModelAndView("myhome", "model", myModel);
    }

    @RequestMapping(value="/add")
	public String addArticle(Model uiModel) {
		
		return "adda";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addArticle(HttpServletRequest httpServletRequest) {
		
		Article product = new Article();
		product.settitle(httpServletRequest.getParameter("title"));
		product.setcontent(httpServletRequest.getParameter("content"));
		  HttpSession session=httpServletRequest.getSession();
	        long nu=((User) session.getAttribute("user")).getnumber();
		product.setuupload(nu);
		this.articleManager.addArticle(product);
		
		return "redirect:/article/myhome.htm";
	}
	
	@RequestMapping(value="/edit/{number}", method=RequestMethod.GET)
	public String editArticle(@PathVariable("number") Long number, Model uiModel) {
		
		Article product = this.articleManager.getArticleById(number);
		uiModel.addAttribute("article", product);
		
		return "edita";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editArticle(@Valid Article article,HttpServletRequest httpServletRequest) {
		 HttpSession session=httpServletRequest.getSession();
	        long nu=((User) session.getAttribute("user")).getnumber();
		article.setuupload(nu);
		this.articleManager.updateArticle(article);
		System.out.println(article.getnumber());
		
		return "redirect:/article/myhome.htm";
	}
	
	@RequestMapping(value="/delete/{number}", method=RequestMethod.GET)
	public String deleteArticle(@PathVariable("number") Long number) {
		
		this.articleManager.deleteArticle(number);
		
		return "redirect:/article/myhome.htm";
	}
}

