package au.usyd.elec5619.web;

//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.servlet.mvc.Controller;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.PoManager;
import au.usyd.elec5619.service.UserManager;

public class DistanceController implements Controller{

    protected final Log logger = LogFactory.getLog(getClass());
    private PoManager poManager;
    public void setPoManager(PoManager poManager) {
	    this.poManager = poManager;
	}
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        String now = (new java.util.Date()).toString();
//        logger.info("returning hello view with " + now);

        Map<String, Object> myModel = new HashMap<String, Object>();
//        HttpSession session=request.getSession();
//        long nu=((User) session.getAttribute("user")).getnumber();
        myModel.put("pos", this.poManager.getAllPos());

        return new ModelAndView("distance", "model", myModel);
    }

   

}


