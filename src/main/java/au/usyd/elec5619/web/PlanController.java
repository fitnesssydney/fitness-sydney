package au.usyd.elec5619.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;


import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.Plan;
import au.usyd.elec5619.service.UserManager;
import au.usyd.elec5619.service.PlanManager;

@Controller
@RequestMapping(value="/plan/**")
public class PlanController {
	
   @Resource(name="userManager")
	private UserManager userManager;
   @Resource(name="planManager")
    private PlanManager planManager;

    @RequestMapping(value="/myplan", method=RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//		User user = new User();
//		user.setname(request.getParameter("name"));
//		user.setpw(request.getParameter("pw"));
//		List list = (List) userManager.findByNamePwd(user.getname(),user.getpw());
		//List<User> list = this.userManager.getUsers(user.getname(),user.getpw());
		//List<User> list = this.userManager.getAllUsers();
		HttpSession session = request.getSession();
		 long nu=((User) session.getAttribute("user")).getnumber();
			//user = (User) list.get(0);
			//Article article = (Article) deptInfoDao.findById(user.getDept());
//			httpSession.setAttribute("user", user);  
			 Map<String, Object> map = new HashMap<String, Object>();		
			//map.put("user", user);
			
			map.put("plans", this.planManager.getPlansByuupload(nu)); 
//			uupload=user.getnumber();
			
			//map.put("articles", this.articleManager.getAllArticles()); 
			return new ModelAndView("myplan", "model", map);       
       
    }

	
  
	@RequestMapping(value="/add")
	public String addPlan(Model uiModel) {
		
		return "addp";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addPlan(HttpServletRequest httpServletRequest) {
		
		Plan product = new Plan();
		product.setdate(httpServletRequest.getParameter("date"));
		product.settype(httpServletRequest.getParameter("type"));
		product.setpa(Double.valueOf(httpServletRequest.getParameter("pa")));
		product.setra(Double.valueOf(httpServletRequest.getParameter("ra")));
		  HttpSession session=httpServletRequest.getSession();
	        long nu=((User) session.getAttribute("user")).getnumber();
		product.setuupload(nu);
		this.planManager.addPlan(product);
		
		return "redirect:/plan/myplan.htm";
	}
	
	@RequestMapping(value="/edit/{number}", method=RequestMethod.GET)
	public String editPlan(@PathVariable("number") Long number, Model uiModel) {
		
		Plan product = this.planManager.getPlanById(number);
		uiModel.addAttribute("plan", product);
		
		return "editp";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editPlan(@Valid Plan article,HttpServletRequest httpServletRequest) {
		 HttpSession session=httpServletRequest.getSession();
	        long nu=((User) session.getAttribute("user")).getnumber();
		article.setuupload(nu);
		this.planManager.updatePlan(article);
		System.out.println(article.getnumber());
		
		return "redirect:/plan/myplan.htm";
	}
	
	@RequestMapping(value="/delete/{number}", method=RequestMethod.GET)
	public String deletePlan(@PathVariable("number") Long number) {
		
		this.planManager.deletePlan(number);
		
		return "redirect:/plan/myplan.htm";
	}
}


