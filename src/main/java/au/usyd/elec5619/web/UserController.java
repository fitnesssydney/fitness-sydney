package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.UserManager;


@Controller
@RequestMapping(value="/user/**")//
public class UserController {
	
	@Resource(name="userManager")
	private UserManager userManager;

	@RequestMapping(value="/register")
	public String addUser(Model uiModel) {
		
		return "register";
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String addUser(HttpServletRequest httpServletRequest) {
		
		User product = new User();
		product.setname(httpServletRequest.getParameter("description"));
		product.setpw(httpServletRequest.getParameter("price"));
		String pw=product.getpw();
		String compw=httpServletRequest.getParameter("comprice");
		if(pw.equals(compw)){
		this.userManager.addUser(product);
		
		return "redirect:/";
		}else{
			//////////////////////////////////////////////////////////////
		return "redirect:/";
		}
	}
	
//	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
//	public String editProduct(@PathVariable("id") Long id, Model uiModel) {
//		
//		Product product = this.productManager.getProductById(id);
//		uiModel.addAttribute("product", product);
//		
//		return "edit";
//	}
//	
//	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
//	public String editProduct(@Valid Product product) {
//		
//		this.productManager.updateProduct(product);
//		System.out.println(product.getId());
//		
//		return "redirect:/hello.htm";
//	}
//	
//	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
//	public String deleteProduct(@PathVariable("id") Long id) {
//		
//		this.productManager.deleteProduct(id);
//		
//		return "redirect:/hello.htm";
//	}
}
