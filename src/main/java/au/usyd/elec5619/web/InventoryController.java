package au.usyd.elec5619.web;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.usyd.elec5619.service.ArticleManager;
import au.usyd.elec5619.service.ProductManager;
public class InventoryController implements Controller {

    protected final Log logger = LogFactory.getLog(getClass());
    
    private ProductManager productManager;
    private ArticleManager articleManager;

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        String now = (new java.util.Date()).toString();
//        logger.info("returning hello view with " + now);

        Map<String, Object> myModel = new HashMap<String, Object>();
       
        myModel.put("articles", this.articleManager. getAllArticles());

        return new ModelAndView("hello", "model", myModel);
    }


    public void setProductManager(ProductManager productManager) {
        this.productManager = productManager;
    }
    public void setArticleManager(ArticleManager productManager) {
        this.articleManager = productManager;
    }
}

