package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.Product;
import au.usyd.elec5619.domain.User;

@Service(value="articleManager")
@Transactional
public class DatabaseArticleManager implements ArticleManager {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addArticle(Article article) {
		this.sessionFactory.getCurrentSession().save(article);
	}
	
	@Override
	public List getArticlesByuupload(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query=currentSession.createQuery("FROM Article WHERE uupload = '" + id +"'");
		List list = query.list();
		return list;
		
	}
	
	
	@Override
	public void updateArticle(Article product) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		//currentSession.merge(product);
		currentSession.update(product);
	}
	
	
//	@Override
//	public void deleteArticle(long number) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Article product = (Article) currentSession.get(Article.class, number);
//		currentSession.delete(product);
//	}
	@Override
	public void deleteArticle(long number) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Article product = (Article) currentSession.get(Article.class, number);
		currentSession.delete(product);
	}
	@Override
	public List<Article> getAllArticles() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM Article").list();
	}
	@Override
	public Article getArticleById(long number) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Article product = (Article) currentSession.get(Article.class, number);
		return product;
	}

}