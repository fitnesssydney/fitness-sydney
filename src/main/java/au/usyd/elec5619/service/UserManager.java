package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import  au.usyd.elec5619.domain.User;

public interface UserManager extends Serializable{

   // public void increasePrice(int percentage);
    
   // public List<User> getUsers();
    
    public void addUser(User user);
        
    public User getUserById(long id);
        
    public void updateUser(User product);
        
    //public void deleteProduct(long id);
    public List findByNamePwd(String id, String pwd);
    public List<User> getUsers(String name, String pwd);
    public List<User> getAllUsers();
}
