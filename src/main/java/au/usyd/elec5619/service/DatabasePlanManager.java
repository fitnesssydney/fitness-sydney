package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Plan;

import au.usyd.elec5619.domain.User;

@Service(value="planManager")
@Transactional
public class DatabasePlanManager implements PlanManager {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addPlan(Plan plan) {
		this.sessionFactory.getCurrentSession().save(plan);
	}
	
	@Override
	public List getPlansByuupload(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query=currentSession.createQuery("FROM Plan WHERE uupload = '" + id +"'");
		List list = query.list();
		return list;
		
	}
	
	
	@Override
	public void updatePlan(Plan product) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		//currentSession.merge(product);
		currentSession.update(product);
	}
	
	
//	@Override
//	public void deleteArticle(long number) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Article product = (Article) currentSession.get(Article.class, number);
//		currentSession.delete(product);
//	}
	@Override
	public void deletePlan(long number) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Plan product = (Plan) currentSession.get(Plan.class, number);
		currentSession.delete(product);
	}
//	@Override
//	public List<Article> getAllArticles() {
//		return this.sessionFactory.getCurrentSession().createQuery("FROM Article").list();
//	}
	@Override
	public Plan getPlanById(long number) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Plan product = (Plan) currentSession.get(Plan.class, number);
		return product;
	}

}