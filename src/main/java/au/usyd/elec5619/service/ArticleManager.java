package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import  au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.Product;
import au.usyd.elec5619.domain.User;

public interface ArticleManager extends Serializable{

    //public void increasePrice(int percentage);
    
    //public List<Article> getArticles();
    
    public void addArticle(Article article);
        
    public List getArticlesByuupload(long id);
        
    public void updateArticle(Article product);
        
    public void deleteArticle(long number);
   
    public List<Article> getAllArticles();
    
    public Article getArticleById(long id);
    
    
}
