package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import  au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.Plan;
import au.usyd.elec5619.domain.Product;
import au.usyd.elec5619.domain.User;

public interface PlanManager extends Serializable{

    //public void increasePrice(int percentage);
    
    //public List<Article> getArticles();
    
    public void addPlan(Plan plan);
        
    public List getPlansByuupload(long id);
        
    public void updatePlan(Plan plan);
        
    public void deletePlan(long number);
   
    //public List<Article> getAllArticles();
    
    public Plan getPlanById(long id);
    
    
}
