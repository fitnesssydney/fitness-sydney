package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.hibernate.Query;

import au.usyd.elec5619.domain.Product;
import au.usyd.elec5619.domain.User;

@Service(value="userManager")
@Transactional
public class DatabaseUserManager implements UserManager {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public User getUserById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		User product = (User) currentSession.get(User.class, id);
		return product;
	}
	
	@Override
	public void updateUser(User product) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(product);
	}
	@Override
	public List findByNamePwd(String name, String pwd) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query=currentSession.createQuery("FROM User WHERE name = '" + name + "' AND pw = '"
				+ pwd + "'");
		List list = query.list();
		return list;
	}
	@Override
	public List<User> getUsers(String name, String pwd) {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User WHERE name = '" + name + "' AND pw = '"
				+ pwd + "'").list();
	}
//	@Override
//	public void deleteProduct(long id) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Product product = (Product) currentSession.get(Product.class, id);
//		currentSession.delete(product);
//	}

//	@Override
//	public void increasePrice(int percentage) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		List<Product> products = currentSession.createQuery("FROM Product").list();
//		
//		if (products != null) {
//            for (Product product : products) {
//                double newPrice = product.getPrice().doubleValue() * 
//                                    (100 + percentage)/100;
//                product.setPrice(newPrice);
//                currentSession.save(product);
//            }
//        }
//	}

	@Override
	public List<User> getAllUsers() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
	}

	
}