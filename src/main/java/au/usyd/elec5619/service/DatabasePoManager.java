package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.domain.Po;
import au.usyd.elec5619.domain.Product;
import au.usyd.elec5619.domain.User;

@Service(value="poManager")
@Transactional
public class DatabasePoManager implements PoManager {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addPo(Po po) {
		this.sessionFactory.getCurrentSession().save(po);
	}	

	@Override
	public List<Po> getAllPos() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM Po").list();
	}


}