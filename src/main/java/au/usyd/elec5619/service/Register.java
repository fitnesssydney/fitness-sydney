package au.usyd.elec5619.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Register {

    /** Logger for this class and subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private String username;
    private String pw;
    public void setusername(String i) {
        username = i;
        logger.info("username set to " + username);
    }

    public String getusername() {
        return username;
    }
    public void setpw(String i) {
        pw = i;
        logger.info("password set to " + pw);
    }

    public String getpw() {
        return pw;
    }

}
