package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name="po")
public class Po implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="number")
	private long number;
		
	@Column(name="x")
	private String x;
    @Column(name="y")
    private String y;
    
  
    public long getnumber() {
    		 		return number;
    		 	}
    		 
    public void setnumber(long number) {
    		 		this.number = number;
    		 }
    		 
    public String getx() {
        return x;
    }
    
    public void setx(String name) {
        this.x = name;
    }
    
    public String gety() {
        return y;
    }
    
    public void sety(String pw) {
        this.y = pw;
    }
    
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Description: " + x + ";");
        buffer.append("Price: " +y);
        return buffer.toString();
    }
}
