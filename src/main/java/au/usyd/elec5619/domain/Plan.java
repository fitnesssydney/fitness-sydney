package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="plan")
public class Plan implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="number")
	private long number;
		
	@Column(name="date")
	private String date;
    @Column(name="type")
    private String type;
    @Column(name="uupload")
	private long uupload;
    @Column(name="pa")
    private double pa;
    @Column(name="ra")
	private double ra;
    public long getnumber() {
    		 		return number;
    		 	}
    		 
    public void setnumber(long number) {
    		 		this.number = number;
    		 }
    		 
    public String getdate() {
        return date;
    }
    
    public void setdate(String title) {
        this.date = title;
    }
    
    public String gettype() {
        return type;
    }
    
    public void settype(String content) {
        this.type = content;
    }
    public double getpa() {
 		return pa;
 	}
 
   public void setpa(double uupload) {
 		this.pa = uupload;
 }
   public double getra() {
		return ra;
	}

  public void setra(double double1) {
		this.ra = double1;
}
  public long getuupload() {
		return uupload;
	}

 public void setuupload(long uupload) {
		this.uupload = uupload;
}

}
