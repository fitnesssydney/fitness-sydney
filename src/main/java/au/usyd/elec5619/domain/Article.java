package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="article")
public class Article implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="number")
	private long number;
		
	@Column(name="title")
	private String title;
    @Column(name="content")
    private String content;
    @Column(name="uupload")
	private long uupload;
  
    public long getnumber() {
    		 		return number;
    		 	}
    		 
    public void setnumber(long number) {
    		 		this.number = number;
    		 }
    		 
    public String gettitle() {
        return title;
    }
    
    public void settitle(String title) {
        this.title = title;
    }
    
    public String getcontent() {
        return content;
    }
    
    public void setcontent(String content) {
        this.content = content;
    }
    public long getuupload() {
 		return uupload;
 	}
 
   public void setuupload(long uupload) {
 		this.uupload = uupload;
 }
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Description: " + title + ";");
        buffer.append("Price: " + content);
        return buffer.toString();
    }
}
