package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="number")
	private long number;
		
	@Column(name="name")
	private String name;
    @Column(name="pw")
    private String pw;
    
  
    public long getnumber() {
    		 		return number;
    		 	}
    		 
    public void setnumber(long number) {
    		 		this.number = number;
    		 }
    		 
    public String getname() {
        return name;
    }
    
    public void setname(String name) {
        this.name = name;
    }
    
    public String getpw() {
        return pw;
    }
    
    public void setpw(String pw) {
        this.pw = pw;
    }
    
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Description: " + name + ";");
        buffer.append("Price: " + pw);
        return buffer.toString();
    }
}
