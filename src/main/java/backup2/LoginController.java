package backup2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.servlet.mvc.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;


import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.Article;
import au.usyd.elec5619.service.UserManager;
import au.usyd.elec5619.service.ArticleManager;

@Controller
@RequestMapping(value="/user/**")
public class LoginController {
	
   @Resource(name="userManager")
	private UserManager userManager;
   @Resource(name="articleManager")
    private ArticleManager articleManager;
private long uupload;
//    public void setUserManager(UserManager userManager) {
//		this.userManager = userManager;
//	}
//    public void setArticleManager(ArticleManager articleManager) {
//		this.articleManager = articleManager;
//	}
    @RequestMapping(value="/login")
	public String addProduct(Model uiModel) {
		
		return "login";
	}
    @RequestMapping(value="/login", method=RequestMethod.POST)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		User user = new User();
		user.setname(request.getParameter("name"));
		user.setpw(request.getParameter("pw"));
		List list = (List) userManager.findByNamePwd(user.getname(),user.getpw());
		//List<User> list = this.userManager.getUsers(user.getname(),user.getpw());
		//List<User> list = this.userManager.getAllUsers();
		if(list.isEmpty()){
			return new ModelAndView("login","error","Wrong username or password");
		}else{
			
			user = (User) list.get(0);
			//Article article = (Article) deptInfoDao.findById(user.getDept());
//			httpSession.setAttribute("user", user);  
			 Map<String, Object> map = new HashMap<String, Object>();		
			map.put("user", user);
			
			map.put("articles", this.articleManager.getArticlesByuupload(user.getnumber())); 
			uupload=user.getnumber();
			//map.put("articles", this.articleManager.getAllArticles()); 
			return new ModelAndView("myhome", "model", map);
		}
       
       
    }

	
  
	@RequestMapping(value="/add")
	public String addArticle(Model uiModel) {
		
		return "adda";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addArticle(HttpServletRequest httpServletRequest,HttpSession httpSession) {
		
		Article product = new Article();
		product.settitle(httpServletRequest.getParameter("title"));
		product.setcontent(httpServletRequest.getParameter("content"));
		product.setuupload(uupload);
		this.articleManager.addArticle(product);
		
		return "redirect:/myhome.htm";
	}
	
	@RequestMapping(value="/edit/{number}", method=RequestMethod.GET)
	public String editArticle(@PathVariable("number") Long number, Model uiModel) {
		
		Article product = this.articleManager.getArticleById(number);
		uiModel.addAttribute("article", product);
		
		return "edita";
	}
	
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editArticle(@Valid Article article) {
		
		this.articleManager.updateArticle(article);
		System.out.println(article.getnumber());
		
		return "redirect:/myhome.htm";
	}
	
	@RequestMapping(value="/delete/{number}", method=RequestMethod.GET)
	public String deleteArticle(@PathVariable("number") Long number) {
		
		this.articleManager.deleteArticle(number);
		
		return "redirect:/myhome.htm";
	}
}
