<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
	
    <script>
	
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: -33.890146, lng: 151.193169}, 
    mapTypeId: google.maps.MapTypeId.TERRAIN
  });

  var PathCoordinates = new google.maps.MVCArray([  
    <c:forEach items="${model.pos}" var="prod">
      new google.maps.LatLng (${prod.x}, ${prod.y}),
      </c:forEach>
   ]);  
 
 var flightPath = new google.maps.Polyline({
    path: PathCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  flightPath.setMap(map);
}

    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfESsFrlSrEgDZyVWRviBm9YqYR9qXiB0&signed_in=true&callback=initMap"></script>
  </body>
</html>