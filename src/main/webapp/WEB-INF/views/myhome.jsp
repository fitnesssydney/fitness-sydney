<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" %>
 <html>
<head>
		<title><fmt:message key="title" /></title>
		<link href="<%=request.getContextPath()%>/resources/css/sidebar.css" rel="stylesheet" type="text/css" />
			<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/default.css">
	<script src='http://www.htmleaf.com/assets/js/prefixfree.min.js'></script>
	<style>
	#columns {
		column-width: 320px;
		column-gap: 15px;
	  width: 90%;
		max-width: 1100px;
		margin: 50px auto;
	}

	div#columns figure {
		background: #fefefe;
		border: 2px solid #fcfcfc;
		box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
		margin: 0 2px 15px;
		padding: 15px;
		padding-bottom: 10px;
		transition: opacity .4s ease-in-out;
	  display: inline-block;
	  column-break-inside: avoid;
	}

	div#columns figure img {
		width: 100%; height: auto;
		border-bottom: 1px solid #ccc;
		padding-bottom: 15px;
		margin-bottom: 5px;
	}

	div#columns figure figcaption {
	  font-size: .9rem;
		color: #444;
	  line-height: 1.5;
	}

	div#columns small { 
	  font-size: 1rem;
	  float: right; 
	  text-transform: uppercase;
	  color: #aaa;
	} 

	div#columns small a { 
	  color: #666; 
	  text-decoration: none; 
	  transition: .4s color;
	}

	div#columns:hover figure:not(:hover) {
		opacity: 0.4;
	}

	@media screen and (max-width: 750px) { 
	  #columns { column-gap: 0px; }
	  #columns figure { width: 100%; }
	}
	</style>
	</head>
	<body>
<div class="sidebar">

        <ul>
            <li class="listItemA"><a href="<c:url value="/"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">HOME</a></li>

            <li class="listItemA"><a href="<c:url value="myhome.htm"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">MY ARTICLE</a></li>

            <li class="listItemA"><a href="<c:url value="/plan/myplan.htm"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">MY PLAN</a></li>
            
            <li class="listItemA"><a href="<c:url value="/record/add.htm"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">RECORD EXERCISE</a></li>

            <li class="listItemA"><a href="<c:url value="/article/add.htm"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">POST ARTICLE</a></li>
            
            <li class="listItemA"><a href="<c:url value="/mydata/mydata.htm"/>"><img alt="icon" height="10" src=
            "<%=request.getContextPath()%>/resources/img/arrow.png" width=
            "40">MY DATA</a></li>
        </ul></div>
	
		<h1 class="right" ><font size="6" color="#1e7db9">
		MY ARTICLE
		</font></h1>
<div class="right">
	<div class="htmleaf-container">
		<div id="columns">
		
				<c:forEach items="${model.articles}" var="prod">	
				

		  <figure>
		  <P><c:out value="${prod.title}" /></p>
			<figcaption><i>$<c:out value="${prod.content}" /></i></figcaption>
			 <a href="user/article/edit/${prod.number }">edit</a> 
			<a href="user/article/delete/${prod.number }">delete</a> 
			</figure>
		</c:forEach>
		
		</div>
	</div>
	</div>
	</body>
 </html>