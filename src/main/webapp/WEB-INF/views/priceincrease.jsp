<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

 <html>
<head>
		<title><fmt:message key="title" /></title>
		<link href="<%=request.getContextPath()%>/resources/css/sidebar.css" rel="stylesheet" type="text/css" />
			<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/default.css">
	<script src='http://www.htmleaf.com/assets/js/prefixfree.min.js'></script>
	<style>
	#columns {
		column-width: 320px;
		column-gap: 15px;
	  width: 90%;
		max-width: 1100px;
		margin: 50px auto;
	}

	div#columns figure {
		background: #fefefe;
		border: 2px solid #fcfcfc;
		box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
		margin: 0 2px 15px;
		padding: 15px;
		padding-bottom: 10px;
		transition: opacity .4s ease-in-out;
	  display: inline-block;
	  column-break-inside: avoid;
	}

	div#columns figure img {
		width: 100%; height: auto;
		border-bottom: 1px solid #ccc;
		padding-bottom: 15px;
		margin-bottom: 5px;
	}

	div#columns figure figcaption {
	  font-size: .9rem;
		color: #444;
	  line-height: 1.5;
	}

	div#columns small { 
	  font-size: 1rem;
	  float: right; 
	  text-transform: uppercase;
	  color: #aaa;
	} 

	div#columns small a { 
	  color: #666; 
	  text-decoration: none; 
	  transition: .4s color;
	}

	div#columns:hover figure:not(:hover) {
		opacity: 0.4;
	}

	@media screen and (max-width: 750px) { 
	  #columns { column-gap: 0px; }
	  #columns figure { width: 100%; }
	}
	</style>
	</head>
	<body>
				<div class="right">
<h1><fmt:message key="priceincrease.heading"/></h1>
<form:form method="post" commandName="priceIncrease">
  <table width="95%" bgcolor="f8f8ff" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="right" width="20%">Increase (%):</td>
        <td width="20%">
          <form:input path="percentage"/>
        </td>
        <td width="60%">
          <form:errors path="percentage" cssClass="error"/>
        </td>
    </tr>
  </table>
  <br>
  <input type="submit" align="center" value="Execute">
</form:form>
<a href="<c:url value="hello.htm"/>">Home</a>
</div>
</body>
</html>